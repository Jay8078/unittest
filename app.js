const express = require('express');
const bodyparser = require('body-parser');
const routes = require('./routes/routes');

const app = express();

app.use(bodyparser.urlencoded({extended: false}));
app.use(express.json());
app.use('/routes', routes );

var server = app.listen('8081',()=>{
    console.log("listening at 8080");
})

module.exports = server;
Kindly update the below-required information in the "README.md" file in the corresponding Repo root level.

README.md File Should Contain following info:

    1.OS & System Configuration
    2.Required Package/Library related Installation Steps with Version.
    3.Compile and Build Steps.
    4.Build Artifacts/Image Output Path.
    5.Kindly add the .gitignore file based on your project.

Repo URL: https://gitlab-dev.vvdntech.com/vvdn_cicd/vvdn_cicd_2022/jaideep.git

Note: Kindly follow the ["Standard SCM Branching Strategies".](https://docs.google.com/document/d/1aqykSw_pBcEN4MGsSysDfFbY9g0Tc0-dklzRM9D_zOM/edit?usp=sharing)

Kindly reach VVDN CI/CD Team (vvdn_cicd@vvdntech.in), if you need any support from them
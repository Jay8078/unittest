const express = require('express');
const res = require('express/lib/response');
const router = express.Router();

const todo = [{
    id: 1,
    name: "Gym",
    completed: false
},
{
    id: 2,
    name: "kai",
    completed: false
}]

router.get('/todos',(req,res)=>{
    res.json(todo)
});

router.get('/todos/:id',(req,res)=>{
    var id = req.params.id;
    var d= todo.find(x=>x.id == id);
    if(d){
        var i= todo.findIndex(x=>x.id == id);
    }
    else{
        res.status(404).send("Not found")
    }
    res.json(todo[i]);
});

router.post('/todos/register',(req,res)=>{
    var id = req.body.id;
    var name = req.body.name;
    var completed = req.body.completed;

    var entry = {
        id : id,
        name: name,
        completed: completed
    }

    todo.push(entry);
    console.log(todo)
    res.status(201).send(entry);
});



module.exports =router;
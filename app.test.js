const request = require("supertest");
const app = require("./app");

describe("todo", () => {
  it("GET/todo --------> arrayOfTodos", () => {
    return request(app)
      .get('/routes/todos')
      .expect("Content-Type", /json/)
      .expect(200)
      .then((response) => {
        expect(response.body).toEqual(
          expect.arrayContaining([
            expect.objectContaining({
              id: expect.any(Number),  
              name: expect.any(String),
              completed: expect.any(Boolean),
            }),
          ])
        );
      });
  });
  it("GET/todo/id --------> specific todo", () => {
    return request(app)
      .get('/routes/todos/1')
      .expect('Content-Type',/json/)
      .expect(200)
      .then((response) => {
        expect(response.body).toEqual(
          expect.objectContaining({
            id: expect.any(Number),
            name: expect.any(String),
            completed: expect.any(Boolean),
          })
        );
      });
  });
  it("POST/todo --------> create a todo", () => {
    return request(app)
      .post('/routes/todos/register')
      .send({
        id: 3,
        name: "jojo",
        completed: false
      })
      .expect("Content-Type", /json/)
      .expect(201)
      .then((response) => {
        expect(response.body).toEqual(
          expect.objectContaining({
            name: "jojo"
          })
        );
      });
  });
  it("GET/todo/id --------> 404 not found", () => {
    return request(app).get('/routes/todos/123').expect(404);
  });
  
});
